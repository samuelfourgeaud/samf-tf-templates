#####################################
# Storage of state file in GCP bucket
#####################################

data "terraform_remote_state" "foo" {
    backend = "gcs" 
    config = {
        bucket  = "wusc-terraform-state"
        prefix  = "terraform/state"
    }
}


module "wusc-host" {
    source = "./modules/wusc-host/"
    region      = var.region
    org_id = var.org_id
    billing_account_id = var.billing_account_id
}



##################################
#       VPCs
##################################

module "wusc-transit-vpc" {
    source = "./modules/wusc-transit-vpc/"
    region      = var.region
    host_project = module.wusc-host.host_project
}

module "wusc-sharedresources-vpc" {
    source = "./modules/wusc-sharedresources-vpc/"
    region      = var.region
    host_project = module.wusc-host.host_project
}

module "wusc-prod-vpc" {
    source = "./modules/wusc-prod-vpc/"
    region      = var.region
    host_project = module.wusc-host.host_project
}

module "wusc-qa-vpc" {
    source = "./modules/wusc-qa-vpc/"
    region      = var.region
    host_project = module.wusc-host.host_project
}

module "wusc-dev-vpc" {
    source = "./modules/wusc-dev-vpc/"
    region      = var.region
    host_project = module.wusc-host.host_project
}

##################################
#       Peerings between VPCs
##################################

module "peerings" {
    source = "./modules/peerings/"
    region      = var.region
    host_project = module.wusc-host.host_project
    sharedresources_vpc = module.wusc-sharedresources-vpc.sharedresources_vpc
    transit_vpc = module.wusc-transit-vpc.transit_vpc
    qa_vpc = module.wusc-qa-vpc.qa_vpc
    dev_vpc = module.wusc-dev-vpc.dev_vpc
    prod_vpc = module.wusc-prod-vpc.prod_vpc
}

##################################
#       Prod/qa/dev env
##################################

module "wusc-envs" {
    source = "./modules/wusc-envs/"
    region      = var.region
    org_id = var.org_id
    billing_account_id = var.billing_account_id
    host_project = module.wusc-host.host_project
    vpc_access_principals = var.vpc_access_principals
}



