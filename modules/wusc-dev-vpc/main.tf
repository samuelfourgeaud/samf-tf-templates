############################
# VPC Creation
############################

resource "google_compute_network" "wusc_dev_vpc" {
    name = "wusc-dev-vpc"
    auto_create_subnetworks = false
}


############################
# Subnetwork creation
############################


## public

resource "google_compute_subnetwork" "dev-public-na-ne1-10-150-64" {
    name          = "dev-public-na-ne1-10-150-64"
    ip_cidr_range = "10.150.64.0/22"
    region        = var.region
    network       = google_compute_network.wusc_dev_vpc.id
    private_ip_google_access = true
    log_config { 
        aggregation_interval = "INTERVAL_10_MIN" 
        flow_sampling = 0.5 
        metadata = "INCLUDE_ALL_METADATA" 
    }
}

## Private

resource "google_compute_subnetwork" "dev-private-na-ne1-10-150-80" {
    name          = "dev-private-na-ne1-10-150-80"
    ip_cidr_range = "10.150.80.0/22"
    region        = var.region
    network       = google_compute_network.wusc_dev_vpc.id
    private_ip_google_access = true
    log_config { 
        aggregation_interval = "INTERVAL_10_MIN" 
        flow_sampling = 0.5 
        metadata = "INCLUDE_ALL_METADATA" 
    }
}

############################
# Future subnetworks
############################

# resource "google_compute_subnetwork" "dev-public-na-ne1-10-150-68" {
#     name          = "dev-public-na-ne1-10-150-68"
#     ip_cidr_range = "10.150.68.0/22"
#     region        = var.region
#     network       = google_compute_network.wusc_dev_vpc.id
# }

# resource "google_compute_subnetwork" "dev-public-na-ne1-10-150-72" {
#     name          = "dev-public-na-ne1-10-150-72"
#     ip_cidr_range = "10.150.72.0/22"
#     region        = var.region
#     network       = google_compute_network.wusc_dev_vpc.id
# }

# resource "google_compute_subnetwork" "dev-public-na-ne1-10-150-76" {
#     name          = "dev-public-na-ne1-10-150-76"
#     ip_cidr_range = "10.150.76.0/22"
#     region        = var.region
#     network       = google_compute_network.wusc_dev_vpc.id
# }


# resource "google_compute_subnetwork" "dev-private-na-ne1-10-150-84" {
#     name          = "dev-private-na-ne1-10-150-84"
#     ip_cidr_range = "10.150.84.0/22"
#     region        = var.region
#     network       = google_compute_network.wusc_dev_vpc.id
# }

# resource "google_compute_subnetwork" "dev-private-na-ne1-10-150-88" {
#     name          = "dev-private-na-ne1-10-150-88"
#     ip_cidr_range = "10.150.88.0/22"
#     region        = var.region
#     network       = google_compute_network.wusc_dev_vpc.id
# }

# resource "google_compute_subnetwork" "dev-private-na-ne1-10-150-92" {
#     name          = "dev-private-na-ne1-10-150-92"
#     ip_cidr_range = "10.150.92.0/22"
#     region        = var.region
#     network       = google_compute_network.wusc_dev_vpc.id
# }

############################
#       Firewall rules
############################

resource "google_compute_firewall" "rule-crosstalk" {
  project     = var.host_project
  name        = "dev-ingress-crosstalk-all"
  network     = google_compute_network.wusc_dev_vpc.id
  description = "Allows communication for internal instances"

  allow {
    protocol  = "all"
  }

  priority = "65534"

  source_ranges = ["10.128.0.0/9"]
  target_tags = ["internal"]
}

# Public SSH

resource "google_compute_firewall" "rule-ssh" {
  project     = var.host_project
  name        = "dev-allow-ssh"
  network     = google_compute_network.wusc_dev_vpc.id
  description = "Public SSH"

  allow {
    protocol  = "tcp"
    ports = ["22"]
  }

  priority = "65532"

  source_ranges = ["0.0.0.0/0"]
  target_tags = ["internal"]
}

# Public RDP

resource "google_compute_firewall" "rule-rdp" {
  project     = var.host_project
  name        = "dev-allow-rdp"
  network     = google_compute_network.wusc_dev_vpc.id
  description = "Public RDP"

  allow {
    protocol  = "tcp"
    ports = ["3389"]
  }

  priority = "65531"

  source_ranges = ["0.0.0.0/0"]
  target_tags = ["internal"]
}

# IAP

resource "google_compute_firewall" "rule-iap" {
  project     = var.host_project
  name        = "dev-allow-iap"
  network     = google_compute_network.wusc_dev_vpc.id
  description = "IAP"

  allow {
    protocol  = "tcp"
    ports = ["22", "3389"]
  }

  priority = "65533"

  source_ranges = ["35.235.240.0/20"]
  target_tags = ["internal"]
}