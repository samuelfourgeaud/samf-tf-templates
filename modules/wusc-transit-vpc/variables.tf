variable "region" {
    type = string
    description = "The region where to deploy the resources"
    default = ""
} 

variable "host_project" {
    type        = string
    description = "The id of the host project"
    default     = ""
}