############################
# VPC Creation
############################

resource "google_compute_network" "wusc_prod_vpc" {
    name = "wusc-prod-vpc"
    auto_create_subnetworks = false
}


############################
# Subnetwork creation
############################


## public

resource "google_compute_subnetwork" "prod-public-na-ne1-10-150-0" {
    name          = "prod-public-na-ne1-10-150-0"
    ip_cidr_range = "10.150.0.0/22"
    region        = var.region
    network       = google_compute_network.wusc_prod_vpc.id
    private_ip_google_access = true
    log_config { 
        aggregation_interval = "INTERVAL_10_MIN" 
        flow_sampling = 0.5 
        metadata = "INCLUDE_ALL_METADATA" 
    }
}


## Private


resource "google_compute_subnetwork" "prod-private-na-ne1-10-150-16" {
    name          = "prod-private-na-ne1-10-150-16"
    ip_cidr_range = "10.150.16.0/22"
    region        = var.region
    network       = google_compute_network.wusc_prod_vpc.id
    private_ip_google_access = true
    log_config { 
        aggregation_interval = "INTERVAL_10_MIN" 
        flow_sampling = 0.5 
        metadata = "INCLUDE_ALL_METADATA" 
    }
}

############################
# Future subnetworks
############################

# resource "google_compute_subnetwork" "prod-public-na-ne1-10-150-4" {
#     name          = "prod-public-na-ne1-10-150-4"
#     ip_cidr_range = "10.150.4.0/22"
#     region        = var.region
#     network       = google_compute_network.wusc_prod_vpc.id
# }

# resource "google_compute_subnetwork" "prod-public-na-ne1-10-150-8" {
#     name          = "prod-public-na-ne1-10-150-8"
#     ip_cidr_range = "10.150.8.0/22"
#     region        = var.region
#     network       = google_compute_network.wusc_prod_vpc.id
# }

# resource "google_compute_subnetwork" "prod-public-na-ne1-10-150-12" {
#     name          = "prod-public-na-ne1-10-150-12"
#     ip_cidr_range = "10.150.12.0/22"
#     region        = var.region
#     network       = google_compute_network.wusc_prod_vpc.id
# }

# resource "google_compute_subnetwork" "prod-private-na-ne1-10-150-20" {
#     name          = "prod-private-na-ne1-10-150-20"
#     ip_cidr_range = "10.150.20.0/22"
#     region        = var.region
#     network       = google_compute_network.wusc_prod_vpc.id
# }

# resource "google_compute_subnetwork" "prod-private-na-ne1-10-150-24" {
#     name          = "prod-private-na-ne1-10-150-24"
#     ip_cidr_range = "10.150.24.0/22"
#     region        = var.region
#     network       = google_compute_network.wusc_prod_vpc.id
# }

# resource "google_compute_subnetwork" "prod-private-na-ne1-10-150-28" {
#     name          = "prod-private-na-ne1-10-150-28"
#     ip_cidr_range = "10.150.28.0/22"
#     region        = var.region
#     network       = google_compute_network.wusc_prod_vpc.id
# }

############################
#       Firewall rules
############################

resource "google_compute_firewall" "crosstalk" {
  project     = var.host_project
  name        = "prod-ingress-crosstalk-all"
  network     = google_compute_network.wusc_prod_vpc.id
  description = "Allows communication for internal instances"

  allow {
    protocol  = "all"
  }

  priority = "65534"

  source_ranges = ["10.128.0.0/9"]
  target_tags = ["internal"]
}

# Public SSH

resource "google_compute_firewall" "rule-ssh" {
  project     = var.host_project
  name        = "prod-allow-ssh"
  network     = google_compute_network.wusc_prod_vpc.id
  description = "Public SSH"

  allow {
    protocol  = "tcp"
    ports = ["22"]
  }

  priority = "65532"

  source_ranges = ["0.0.0.0/0"]
  target_tags = ["internal"]
}

# Public RDP

resource "google_compute_firewall" "rule-rdp" {
  project     = var.host_project
  name        = "prod-allow-rdp"
  network     = google_compute_network.wusc_prod_vpc.id
  description = "Public RDP"

  allow {
    protocol  = "tcp"
    ports = ["3389"]
  }

  priority = "65531"

  source_ranges = ["0.0.0.0/0"]
  target_tags = ["internal"]
}

# IAP

resource "google_compute_firewall" "rule-iap" {
  project     = var.host_project
  name        = "prod-allow-iap"
  network     = google_compute_network.wusc_prod_vpc.id
  description = "IAP"

  allow {
    protocol  = "tcp"
    ports = ["22", "3389"]
  }

  priority = "65533"

  source_ranges = ["35.235.240.0/20"]
  target_tags = ["internal"]
}