variable "region" {
    type = string
    description = "The region where to deploy the resources"
    default = ""
} 

variable "project_base_name" {
    type = string
    description = "The base name of the project"
    default = "wusc"
}

variable "org_id" {
    type = string
    description = "The org ID the projects will live under"
}

variable "billing_account_id" {
    type = string
    description = "The billing account id to use"
}