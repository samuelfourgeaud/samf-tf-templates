############################
#     Project creation
############################


# Generates a random ids for projects
resource "random_id" "id_host" {
  byte_length = 6
}

resource "google_project" "wusc-host" {
  name       = "${var.project_base_name}-host"
  project_id = "${var.project_base_name}-${random_id.id_host.hex}"
  org_id     = var.org_id
  billing_account = var.billing_account_id
  auto_create_network = false
  labels = {
    "environment" = "shared-host"
    "managed-by-terraform" = "true"
  }
}

# Activates the compute engine api (required for shared vpc)

resource "google_project_service" "wusc-host-compute" {
  project = google_project.wusc-host.project_id
  service = "compute.googleapis.com"
  disable_dependent_services = true
}
