############################
# VPC Creation
############################

resource "google_compute_network" "wusc_qa_vpc" {
    name = "wusc-qa-vpc"
    auto_create_subnetworks = false
}


############################
# Subnetwork creation
############################


## public

resource "google_compute_subnetwork" "qa-public-na-ne1-10-150-32" {
    name          = "qa-public-na-ne1-10-150-32"
    ip_cidr_range = "10.150.32.0/22"
    region        = var.region
    network       = google_compute_network.wusc_qa_vpc.id
    private_ip_google_access = true
    log_config { 
        aggregation_interval = "INTERVAL_10_MIN" 
        flow_sampling = 0.5 
        metadata = "INCLUDE_ALL_METADATA" 
    }
}

## Private


resource "google_compute_subnetwork" "qa-private-na-ne1-10-150-48" {
    name          = "qa-private-na-ne1-10-150-48"
    ip_cidr_range = "10.150.48.0/22"
    region        = var.region
    network       = google_compute_network.wusc_qa_vpc.id
    private_ip_google_access = true
    log_config { 
        aggregation_interval = "INTERVAL_10_MIN" 
        flow_sampling = 0.5 
        metadata = "INCLUDE_ALL_METADATA" 
    }
}

############################
# Future subnetworks
############################

# resource "google_compute_subnetwork" "qa-public-na-ne1-10-150-36" {
#     name          = "qa-public-na-ne1-10-150-36"
#     ip_cidr_range = "10.150.36.0/22"
#     region        = var.region
#     network       = google_compute_network.wusc_qa_vpc.id
# }

# resource "google_compute_subnetwork" "qa-public-na-ne1-10-150-40" {
#     name          = "qa-public-na-ne1-10-150-40"
#     ip_cidr_range = "10.150.40.0/22"
#     region        = var.region
#     network       = google_compute_network.wusc_qa_vpc.id
# }

# resource "google_compute_subnetwork" "qa-public-na-ne1-10-150-4" {
#     name          = "qa-public-na-ne1-10-150-44"
#     ip_cidr_range = "10.150.44.0/22"
#     region        = var.region
#     network       = google_compute_network.wusc_qa_vpc.id
# }


# resource "google_compute_subnetwork" "qa-private-na-ne1-10-150-52" {
#     name          = "qa-private-na-ne1-10-150-52"
#     ip_cidr_range = "10.150.52.0/22"
#     region        = var.region
#     network       = google_compute_network.wusc_qa_vpc.id
# }

# resource "google_compute_subnetwork" "qa-private-na-ne1-10-150-56" {
#     name          = "qa-private-na-ne1-10-150-56"
#     ip_cidr_range = "10.150.56.0/22"
#     region        = var.region
#     network       = google_compute_network.wusc_qa_vpc.id
# }

# resource "google_compute_subnetwork" "qa-private-na-ne1-10-150-60" {
#     name          = "qa-private-na-ne1-10-150-60"
#     ip_cidr_range = "10.150.60.0/22"
#     region        = var.region
#     network       = google_compute_network.wusc_qa_vpc.id
# }

############################
#       Firewall rules
############################

resource "google_compute_firewall" "rule-crosstalk" {
  project     = var.host_project
  name        = "qa-ingress-crosstalk-all"
  network     = google_compute_network.wusc_qa_vpc.id
  description = "Allows communication for internal instances"

  allow {
    protocol  = "all"
  }

  priority = "65534"

  source_ranges = ["10.128.0.0/9"]
  target_tags = ["internal"]
}

# Public SSH

resource "google_compute_firewall" "rule-ssh" {
  project     = var.host_project
  name        = "qa-allow-ssh"
  network     = google_compute_network.wusc_qa_vpc.id
  description = "Public SSH"

  allow {
    protocol  = "tcp"
    ports = ["22"]
  }

  priority = "65532"

  source_ranges = ["0.0.0.0/0"]
  target_tags = ["internal"]
}

# Public RDP

resource "google_compute_firewall" "rule-rdp" {
  project     = var.host_project
  name        = "qa-allow-rdp"
  network     = google_compute_network.wusc_qa_vpc.id
  description = "Public RDP"

  allow {
    protocol  = "tcp"
    ports = ["3389"]
  }

  priority = "65531"

  source_ranges = ["0.0.0.0/0"]
  target_tags = ["internal"]
}

# IAP

resource "google_compute_firewall" "rule-iap" {
  project     = var.host_project
  name        = "qa-allow-iap"
  network     = google_compute_network.wusc_qa_vpc.id
  description = "IAP"

  allow {
    protocol  = "tcp"
    ports = ["22", "3389"]
  }

  priority = "65533"

  source_ranges = ["35.235.240.0/20"]
  target_tags = ["internal"]
}