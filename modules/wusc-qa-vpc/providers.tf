provider "google" {
    project     = var.host_project
    region      = var.region
}