resource "google_compute_network_peering" "sharedresources-transit" {
  name         = "sharedresources-transit"
  network      = var.sharedresources_vpc
  peer_network = var.transit_vpc
}

resource "google_compute_network_peering" "transit-sharedresources" {
  name         = "transit-sharedresources"
  network      = var.transit_vpc
  peer_network = var.sharedresources_vpc
}

resource "google_compute_network_peering" "prod-transit" {
  name         = "prod-transit"
  network      = var.prod_vpc
  peer_network = var.transit_vpc
}

resource "google_compute_network_peering" "transit-prod" {
  name         = "transit-prod"
  network      = var.transit_vpc
  peer_network = var.prod_vpc
}

resource "google_compute_network_peering" "prod-sharedresources" {
  name         = "prod-sharedresources"
  network      = var.prod_vpc
  peer_network = var.sharedresources_vpc
}

resource "google_compute_network_peering" "sharedresources-prod" {
  name         = "sharedresources-prod"
  network      = var.sharedresources_vpc
  peer_network = var.prod_vpc
}

resource "google_compute_network_peering" "qa-transit" {
  name         = "qa-transit"
  network      = var.qa_vpc
  peer_network = var.transit_vpc
}

resource "google_compute_network_peering" "transit-qa" {
  name         = "transit-qa"
  network      = var.transit_vpc
  peer_network = var.qa_vpc
}

resource "google_compute_network_peering" "qa-sharedresources" {
  name         = "qa-sharedresources"
  network      = var.qa_vpc
  peer_network = var.sharedresources_vpc
}

resource "google_compute_network_peering" "sharedresources-qa" {
  name         = "sharedresources-qa"
  network      = var.sharedresources_vpc
  peer_network = var.qa_vpc
}

resource "google_compute_network_peering" "dev-transit" {
  name         = "dev-transit"
  network      = var.dev_vpc
  peer_network = var.transit_vpc
}

resource "google_compute_network_peering" "transit-dev" {
  name         = "transit-dev"
  network      = var.transit_vpc
  peer_network = var.dev_vpc
}

resource "google_compute_network_peering" "dev-sharedresources" {
  name         = "dev-sharedresources"
  network      = var.dev_vpc
  peer_network = var.sharedresources_vpc
}

resource "google_compute_network_peering" "sharedresources-dev" {
  name         = "sharedresources-dev"
  network      = var.sharedresources_vpc
  peer_network = var.dev_vpc
}
