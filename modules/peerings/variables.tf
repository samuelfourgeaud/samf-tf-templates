variable "region" {
    type = string
    description = "The region where to deploy the resources"
    default = ""
} 

variable "host_project" {
    type        = string
    description = "The id of the host project"
    default     = ""
}

variable "transit_vpc" {
    type        = string
    description = "The self link to the transit VPC"
}

variable "sharedresources_vpc" {
    type        = string
    description = "The self link to the sharedresources VPC"
}

variable "qa_vpc" {
    type        = string
    description = "The self link to the sharedresources VPC"
}

variable "dev_vpc" {
    type        = string
    description = "The self link to the sharedresources VPC"
}

variable "prod_vpc" {
    type        = string
    description = "The self link to the sharedresources VPC"
}