############################
#     Projects creation
############################


# Generates a random ids for projects
resource "random_id" "id_prod" {
  byte_length = 6
}

resource "random_id" "id_qa" {
  byte_length = 6
}

resource "random_id" "id_dev" {
  byte_length = 6
}




resource "google_project" "wusc-prod" {
  name       = "${var.project_base_name}-prod"
  project_id = "${var.project_base_name}-${random_id.id_prod.hex}"
  org_id     = var.org_id
  billing_account = var.billing_account_id
  auto_create_network = false
  labels = {
    "environment" = "prod"
    "managed-by-terraform" = "true"
  }
}

# Activates the compute engine api (required for shared vpc)

resource "google_project_service" "wusc-prod-compute" {
  project = google_project.wusc-prod.project_id
  service = "compute.googleapis.com"
  disable_dependent_services = true
}

resource "google_project" "wusc-qa" {
  name       = "${var.project_base_name}-qa"
  project_id = "${var.project_base_name}-${random_id.id_qa.hex}"
  org_id     = var.org_id
  billing_account = var.billing_account_id
  auto_create_network = false
  labels = {
    "environment" = "qa"
    "managed-by-terraform" = "true"
  }
}

resource "google_project_service" "wusc-qa-compute" {
  project = google_project.wusc-qa.project_id
  service = "compute.googleapis.com"
  disable_dependent_services = true
}


resource "google_project" "wusc-dev" {
  name       = "${var.project_base_name}-dev"
  project_id = "${var.project_base_name}-${random_id.id_dev.hex}"
  org_id     = var.org_id
  billing_account = var.billing_account_id
  auto_create_network = false
  labels = {
    "environment" = "dev"
    "managed-by-terraform" = "true"
  }
}

resource "google_project_service" "wusc-dev-compute" {
  project = google_project.wusc-dev.project_id
  service = "compute.googleapis.com"
  disable_dependent_services = true
}


############################
# Waiting for compute engine API
############################

resource "time_sleep" "wait_300_seconds" {
  depends_on = [google_project_service.wusc-prod-compute, google_project_service.wusc-dev-compute, google_project_service.wusc-qa-compute]

  create_duration = "300s"
}


############################
#     Shared VPC
############################

# A host project provides network resources to associated service projects.
resource "google_compute_shared_vpc_host_project" "host" {
  project = var.host_project
}

resource "google_compute_shared_vpc_service_project" "wusc-prod-vpc" {
  depends_on = [time_sleep.wait_300_seconds]
  host_project    = google_compute_shared_vpc_host_project.host.project
  service_project = google_project.wusc-prod.project_id
}

resource "google_compute_shared_vpc_service_project" "wusc-qa-vpc" {
  depends_on = [time_sleep.wait_300_seconds]
  host_project    = google_compute_shared_vpc_host_project.host.project
  service_project = google_project.wusc-qa.project_id

}

resource "google_compute_shared_vpc_service_project" "wusc-dev-vpc" {
  depends_on = [time_sleep.wait_300_seconds]
  host_project    = google_compute_shared_vpc_host_project.host.project
  service_project = google_project.wusc-dev.project_id
}

# Permissions
resource "google_compute_subnetwork_iam_binding" "prod-public-na-ne1-10-150-0" {

  subnetwork = "prod-public-na-ne1-10-150-0"
  role = "roles/compute.networkUser"
  members = var.vpc_access_principals
}

resource "google_compute_subnetwork_iam_binding" "dev-private-na-ne1-10-150-80" {

  subnetwork = "dev-private-na-ne1-10-150-80"
  role = "roles/compute.networkUser"
  members = var.vpc_access_principals
}