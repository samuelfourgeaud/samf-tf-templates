variable "region" {
    type = string
    description = "The region where to deploy the resources"
    default = ""
} 

variable "project_base_name" {
    type = string
    description = "The base name of the project"
    default = "wusc"
}

variable "org_id" {
    type = string
    description = "The org ID the projects will live under"
}

variable "billing_account_id" {
    type = string
    description = "The billing account id to use"
}

variable "host_project" {
    type        = string
    description = "The id of the host project"
    default     = ""
}

variable "vpc_access_principals" {
    type = list(string)
    description = "Google group given access to all VPCs"
    default = ["",]
}