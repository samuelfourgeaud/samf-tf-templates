# How to deploy?

- Create a new GCP project and name it "wusc-devops".
- Within that project, create a storage bucket named "wusc-terraform-state". This will be used to store the terraform state file.
- Create a cloud source repository and clone this repo into it.
- Edit the terraform.tfvars file with the correct organization id and billing account.
- Open Cloud Shell and clone the repo with the command "gcloud source repos clone REPO_NAME".
- cd into the folder REPO_NAME.
- run "terraform init" followed by "terraform apply" to deploy.
