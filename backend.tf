terraform {
    required_version = ">=0.14"
    backend "gcs" {
        bucket  = "wusc-terraform-state"
        prefix  = "terraform/state"
  }
}

